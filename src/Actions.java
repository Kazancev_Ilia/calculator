import com.sun.istack.internal.NotNull;

import java.util.Scanner;

public  class Actions {

   public static @NotNull int selectAction(){
      System.out.println("Введите номер действия");
      System.out.println("1 - Сложение");
      System.out.println("2 - Умножение");

      Scanner in = new Scanner(System.in);
      return  in.nextInt();
   }
}